# Práctica de CI/CD y Ciencia Abierta con GitLab
En esta tercera parte, me he registrado en Gitlab y he creado un repositorio vacío. En un primer intento, creé un repositorio privado e intenté trabajar con él, pero tras varios problemas a la hora de clonarlo desde el pipeline de Gitlab, decidí trabajar con un repositorio público, que se encuentra disponible en este enlace: https://gitlab.com/diego.buzonperez/gitlab

En la raíz del repositorio he subido el conjunto de datos con el que voy a trabajar. También he creado el [script](https://gitlab.com/diego.buzonperez/gitlab/-/blob/main/dibuja-graficas.py?ref_type=heads) encargado de dibujar las gráficas y el archivo de configuración del [pipeline](https://gitlab.com/diego.buzonperez/gitlab/-/blob/main/.gitlab-ci.yml?ref_type=heads) de GitLab.
En la carpeta *documentacion-generada* he creado un [script](https://gitlab.com/diego.buzonperez/gitlab/-/blob/main/documentacion-generada/genera-documentacion.py?ref_type=heads) para generar la documentación en formato HTML a partir de un fichero .md

A continuación explico, a través de comentarios en el código, el funcionamiento de cada archivo:

#### .gitlab-ci.yaml
```
stages:
  - "Clonar repositorio de Gitlab"
  - Build

# En esta primera fase, clono el repositorio de gitlab donde se encuentran los scripts y el fichero con los datos
Archivos clonados:
  stage: "Clonar repositorio de Gitlab"
  image: gitscm/git # Utilizo una máquina virtual con git instalado
  variables:
    GIT_STRATEGY: none
  script:
    - git clone https://gitlab.com/diego.buzonperez/gitlab.git # Hago una copia del repositorio
  artifacts:
    paths:
      - ./gitlab/ # Ruta en la que se encuentran los ficheros descargados

Graficas y documentacion:
  stage: Build
  image: python # Utilizo una máquina virtual con python
  dependencies: ['Archivos clonados'] # Indico que se utilicen los ficheros descargados en la etapa anterior
  script:
    - pip install matplotlib # Las dos dependencias siguientes son necesarias para dibujar la gráfica y generar un archivo .html
    - pip install markdown
    - python3 ./gitlab/dibuja-graficas.py # se ejecutan los scripts para dibujar la gráfica y generar un archivo .html
    - python3 ./gitlab/documentacion-generada/genera-documentacion.py
  artifacts:
    paths:
      - .
```

#### dibuja-graficas.py
```
import matplotlib.pyplot as plt 
import csv 
  
x = [] # Este vector contiene los valores que va a tomar el eje X
y = [] # Este vector contiene los valores que va a tomar el eje Y
contador = 1 # Este contador se usa para establecer los valores del eje X

# Lectura del fichero de datos
with open('./gitlab/SensorData.csv','r') as csvfile: 
    plots = csv.reader(csvfile, delimiter = ',')
      
    for row in plots: 
        x.append(row[5]) # se añaden los valores que forman el eje X
        y.append(contador) # se añaden los valores que forman el eje Y, que serán: 1,2,3,4,5,6,...
        contador = contador + 1

# Configuración de la gráfica: título, nombre de los ejes y leyenda
plt.bar(x, y, color = 'g', width = 0.72, label = "Temperatura") 
plt.xlabel('Tiempo') 
plt.ylabel('Temperatura') 
plt.title('Temperatura a lo largo del tiempo') 
plt.legend() 

# Finalmente se genera un archivo .png que muestra la gráfica
plt.savefig('grafica_1.png')
```

#### genera-documentacion.py
El siguiente código genera un fichero .html a partir del README.md que se encuentra en la ruta ./gitlab/documentacion-generada del repositorio clonado en la primera etapa del pipeline
```
import markdown

# Se lee y guarda el contenido del README.md como un string
with open('./gitlab/documentacion-generada/README.md', 'r') as f:
    markdown_string = f.read() 

# Se genera un nuevo string en fomato .html a partir del string anterior
html_string = markdown.markdown(markdown_string) 

# Para terminar se crea un archivo .html
with open('documentacion.html', 'w') as f:
    f.write(html_string) 
```