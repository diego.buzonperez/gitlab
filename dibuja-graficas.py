import matplotlib.pyplot as plt 
import csv 
  
x = [] 
y = [] 
contador = 1
  
with open('./gitlab/SensorData.csv','r') as csvfile: 
    plots = csv.reader(csvfile, delimiter = ',')
      
    for row in plots: 
        x.append(row[5]) 
        y.append(contador)
        contador = contador + 1
  
plt.bar(x, y, color = 'g', width = 0.72, label = "Temperatura") 
plt.xlabel('Tiempo') 
plt.ylabel('Temperatura') 
plt.title('Temperatura a lo largo del tiempo') 
plt.legend() 
plt.savefig('grafica_1.png')
