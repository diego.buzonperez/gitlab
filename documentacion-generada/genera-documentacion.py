import markdown

with open('./gitlab/documentacion-generada/README.md', 'r') as f:
    markdown_string = f.read()

html_string = markdown.markdown(markdown_string)

with open('documentacion.html', 'w') as f:
    f.write(html_string)